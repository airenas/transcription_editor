# Transkripcijų redaktorius
## Apie
Transkripcijų redaktorius - tai interneto naršyklėje veikianti SPA (single page application) programa, leidžianti sinchroniškai išklausyti redaguojamą tekstą atitinkantį garso segmentą fonogramoje.
## Programos techniniai reikalavimai
Moderni naršyklė, palaikanti JavaScript ir HTML5 (Internet Explorer naršyklės nėra palaikomos).
Programos paleidimui ir kompiliavimui naudojamas node arba yarn.
## Projekto diegimas
```
yarn install
npm run install
```

### Kompiliavimas ir development aplinkos paleidimas
```
yarn serve
npm run serve
```

### Production versijos kompiliavimas
```
yarn build
npm run build
```

### Docker konteinerio sukūrimas/patalpinimas repozitorijoje

Sukompiliuokite Production versiją. Prisijunkite su docker prie *semantikadocker.vdu.lt* repozitorijos. Tada:

```bash
npm run dbuild
npm run dpush
```
