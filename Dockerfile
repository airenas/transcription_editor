FROM nginx:1.17.9

LABEL Description="This image is used to start the transcription editor hosting service" \ 
      Maintainer="https://bitbucket.org/semantika2/transcription_editor" Vendor="https://bitbucket.org/semantika2/transcription_editor"

COPY dist /usr/share/nginx/html
COPY docker/default.conf /etc/nginx/conf.d/default.conf

