import Vue from 'vue';
import VueRouter from 'vue-router';
import i18n from '../i18n'

Vue.use(VueRouter);

const routes = [
  {
    path: "/:locale",
    component: {
      template: "<router-view></router-view>"
    },
    beforeEnter: (to, from, next) => {
      const locale = to.params.locale;
      const supported_locales = process.env.VUE_APP_I18N_SUPPORTED_LOCALE.split(',');
      if (!supported_locales.includes(locale)) return next(process.env.VUE_APP_I18N_LOCALE);
      if (i18n.locale !== locale) {
        i18n.locale = locale;
      }
      return next();
    },
    children: [
      {
        path: '',
        name: 'import',
        component: () => import('@/components/DataLoaders/FileImport.vue')
      },
      {
        path: 'transcription-text',
        name: 'transcription-text',
        component: () => import('@/components/TranscriptionText.vue'),
      },
      {
        path: 'transcription-edit',
        name: 'transcription-edit',
        component: () => import('@/components/TranscriptionEdit.vue')
      },
      {
        path: 'remote-import/:lattice/:phonogram',
        name: 'remote-import',
        component: () => import('@/components/DataLoaders/RemoteImport.vue')
      }
    ]
  },
  {
    path: '*',
    redirect() {
      return process.env.VUE_APP_I18N_LOCALE;
    }
  }
];

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes,
});

export default router;
