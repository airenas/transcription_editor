import Vue from 'vue';
import Vuex from 'vuex';
import {RootState} from '@/Interfaces/RootState';
import {DeletedSectionInterface, SectionInterface} from '@/Interfaces/SectionInterface';
import {WordInterface} from "@/Interfaces/WordInterface";
import UndoService from "@/Services/UndoService";
import SelectService from "@/Services/SelectService";
const clone = require('rfdc')()

Vue.use(Vuex);

export default new Vuex.Store<RootState>({
  state: {
    media: null,
    mediaObjectUrl: null,
    mediaIsPlaying: false,
    currentTime: null,
    sections: null,
    deletedSection: null,
    insertedSectionId: null,
    currentPage: 1,
    perPage: null,
    mediaLength: null,
    playingMediaStart: null,
    playingMediaEnd: null,
    needToSelectPlayingSegments: true,
    transcriptionLength: null,
    selectedWordIds: null,
    selectedSectionId: null,
    fileName: null,
    undoQueue: null,
    redoQueue: null,
    caretPosition: null,
    speakers: null,
    email: null,
    loadedStorageEntryId: null,
    modalVisible: false,
    extendPlayingRange: false,
    scrollIntoViewEnabled: true
  },

  getters: {
    media: state => state.media,
    currentTime: state => state.currentTime || 0,
    sections: (state:RootState) => state.sections ? state.sections : [],
    pageCount: state => state.sections ? Math.ceil(state.sections.length / state.perPage) : 1,
    perPage: state => state.perPage ? state.perPage : state.sections.length,                      //@deprecated
    currentPage: state => state.currentPage ? state.currentPage : 1,                              //@deprecated
    mediaLength: state => state.mediaLength,
    playingMediaStart: state => state.playingMediaStart,
    playingMediaEnd: state => state.playingMediaEnd,
    needToSelectPlayingSegments: state => state.needToSelectPlayingSegments,
    transcriptionLength: state => state.transcriptionLength,
    primaryWords: (state: RootState) =>  (sectionId?: number) => {
      if (!state.sections || (sectionId && !state.sections.find(section => section.id === sectionId))) {
        return null;
      }

      return !isNaN(sectionId) ? 
        //sectionId nurodytas, grąžinam tik šios sekcijos žodžius
        state.sections.find(section => section.id === sectionId).words.map(word => word.content)
        : 
        //sectionId nenurodytas, grąžinam visų sekcijų žodžius
        state.sections.map(section => {return section.words.map(word => word.content)})
    },
    selectedWordIds: state => state.selectedWordIds ? [...state.selectedWordIds] : null,
    selectedWords: (state: RootState): WordInterface[] => {
      if (state.selectedWordIds === null || state.selectedWordIds.length === 0 || state.selectedSectionId === null) {
        return null;
      }
      const sectionWords = state.sections.find(section => section.id === state.selectedSectionId).words;
      if (!sectionWords) {
        return null;
      }
      let selectedWords = [];
      for (const wordId of state.selectedWordIds) {
        if (!sectionWords.hasOwnProperty(wordId)) {
          continue;
        }
        selectedWords.push({...sectionWords[wordId]});
      }
      return selectedWords.length ? selectedWords : null;
    },
    selectedSectionId: state => state.selectedSectionId,
    selectedSection: (state: RootState): SectionInterface => {
      if (state.selectedSectionId === null) {
        return null
      }
      return state.sections.find(section => section.id === state.selectedSectionId)
    },
    selectedSectionWords: (state: RootState) => (sectionId?: number): WordInterface[] => {
      sectionId = !isNaN(sectionId) ? sectionId : state.selectedSectionId;
      if (sectionId === null) {
        return null;
      }
      const neededSection = state.sections.find(section => section.id === sectionId);
      if (!neededSection) {
        return null;
      }

      return neededSection.words.map(word => {
        return {...word} //giliai kopinam visą info
      });
    },
    fileName: state => state.fileName ? state.fileName : '',
    undoQueue: state => state.undoQueue ? state.undoQueue.map(undoService => Object.assign({}, undoService)) : null,
    redoQueue: state => state.redoQueue ? state.redoQueue.map(undoService => Object.assign({}, undoService)) : null,
    recentUndoAction: state => state.undoQueue ? Object.assign({}, state.undoQueue[0]): null,
    recentRedoAction: state => state.redoQueue ? Object.assign({}, state.redoQueue[0]): null,
    caretPosition: state => state.caretPosition ? Object.assign({}, state.caretPosition) : null,
    speakers: state => state.speakers ? [...state.speakers] : null,
    email: state => state.email ? state.email.slice() : null,
    loadedStorageEntryId: state => state.loadedStorageEntryId ? JSON.parse(JSON.stringify(state.loadedStorageEntryId)) : null,
    deletedSection: state => state.deletedSection,
    insertedSectionId: state => state.insertedSectionId,
    firstWordSelected: (state: RootState): boolean => {
      if (state.selectedSectionId === null || !state.selectedWordIds || !state.selectedWordIds.length) {
        return false;
      }
      return state.selectedWordIds.includes(0);
    },
    lastWordSelected: (state: RootState): boolean => {
      if (state.selectedSectionId === null || !state.selectedWordIds || !state.selectedWordIds.length) {
        return false;
      }
      const mySelectedSectionObject = state.sections.find(section => section.id === state.selectedSectionId);
      if (! mySelectedSectionObject || typeof mySelectedSectionObject.words === 'undefined') {
        return false;
      }
      return state.selectedWordIds.includes(mySelectedSectionObject.words.length - 1);
    },
    mediaObjectUrl: (state: RootState): string => {
      return state.mediaObjectUrl;
    },
    modalIsVisible: state => state.modalVisible,
    mediaIsPlaying: state => state.mediaIsPlaying,
    extendPlayingRange: state => state.extendPlayingRange,
    scrollIntoViewEnabled: state => state.scrollIntoViewEnabled
  },
  mutations: {
    /* užsetinami transkripcijos duomenys */
    setSections (state, sections: SectionInterface[]) {
      state.sections = sections;
    },

    /* užsetinamas media */
    setMedia (state, playableMedia: HTMLMediaElement) {
      state.media = playableMedia;
      if (!playableMedia) {
        this.commit('setMediaLength', null)
      }
    },

    setMediaObjectUrl (state, mediaObjectUrl: string) {
      state.mediaObjectUrl = mediaObjectUrl;
    },

    updateCurrentTime (state) {
      state.currentTime = state.media?.currentTime || 0;
    },

    /** importuoto media failo trukmė sekundėmis */
    setMediaLength (state, mediaLengthInSeconds: number) {
      state.mediaLength = mediaLengthInSeconds;
    },

    /** importuoto transkripcijos failo ilgis sekundėmis */
    setTranscriptionLength (state, transcriptionLength: number) {
      state.transcriptionLength = transcriptionLength;
    },

    /**
     * pažymėti žodžiai
     * @param state
     * @param selectedWordIds
     */
    setSelectedWordIds (state, selectedWordIds: number[]) {
      if (!state.sections || !state.sections.length) {
        this.commit('setSelectedSectionId', null);
        selectedWordIds = null;
      }
      state.selectedWordIds = selectedWordIds;
      this.dispatch('renewMediaCurrentTimeBySelectedWords');
    },

    /**
     * pažymima aktyvi sekcija
     * @param state 
     * @param {number} selectedSectionId 
     */
    setSelectedSectionId (state, selectedSectionId: number) {
      if (!state.sections || !state.sections.length || isNaN(selectedSectionId)) {
        selectedSectionId = null;
      }
      state.selectedSectionId = selectedSectionId;
    },

    /**
     * išsaugomi modifikuoti žodžiai aktyvioje sekcijoje
     * @param state
     * @param words
     */
    replaceActiveSectionWords (state, words: WordInterface[]) {
      if (!words) {
        return;
      }
      this.commit('setUndoQueue', UndoService.getUpdatedUndoQueue(words));
      this.commit('setRedoQueue', null);
      const activeSection = state.sections.find(section => section.id === state.selectedSectionId);
      activeSection.words = words;
      activeSection.start = words[0].start;
      activeSection.end = words[words.length - 1].end;
    },

    undoUserAction(state) { //atliekama undo operacija
      if (!state.undoQueue || !state.undoQueue.length) {
        return;
      }
      const undoOperation = state.undoQueue.shift();
      const wordsForUndo = undoOperation.getSectionWordsForUndo();
      if (!wordsForUndo) {
        return;
      }
      const undoSectionKey = state.sections.findIndex(section => section.id === undoOperation.sectionId);
      if (undoSectionKey < 0) {
        return;
      }
      this.commit('setRedoQueue', UndoService.getUpdatedRedoQueue(wordsForUndo, undoOperation)); //updatinam redo eilę

      state.sections[undoSectionKey].words = wordsForUndo; //atliekam sekcijos žodžių pakeitimus

      //jei reikia, atstatom ištrintą sekciją
      if (undoOperation.deletedSection) {
        state.sections.splice(undoOperation.deletedSection.key, 0, undoOperation.deletedSection.section);
        this.dispatch('deleteEmptySectionsAndOrderSectionIds');
        this.commit('setRedoQueue', null); //yra sekciju jungimas, del to mes trinam redo eile
      } else if (undoOperation.insertedSectionKey !== null && undoOperation.insertedSectionKey > -1) {
        state.sections.splice(undoOperation.insertedSectionKey, 1);
        this.dispatch('deleteEmptySectionsAndOrderSectionIds');
        this.commit('setRedoQueue', null); //yra sekcijos skaidymas, del to mes trinam redo eile
      }
    },

    redoUserAction(state) {
      if (!state.redoQueue || !state.redoQueue.length) {
        return;
      }
      const undoOperation = state.redoQueue.shift();
      const wordsForRedo = undoOperation.getSectionWordsForUndo();
      if (!wordsForRedo) {
        return;
      }
      const currentSectionKey = state.sections.findIndex(section => section.id === undoOperation.sectionId);
      this.commit('setUndoQueue', UndoService.getUpdatedUndoQueue(wordsForRedo, undoOperation));

      state.sections[currentSectionKey].words = wordsForRedo;
    },

    updateFileName (state, fileName: string) {
      state.fileName = fileName;
    },

    setUndoQueue(state, undoQueue:UndoService[]) {
      state.undoQueue = undoQueue;
    },

    setRedoQueue(state, redoQueue:UndoService[]) {
      state.redoQueue = redoQueue;
    },

    rememberCaretPosition (state, selectService: SelectService) {
      state.caretPosition = selectService;
    },

    setSpeakers (state, speakers: string[]) {
      if (speakers) {
        speakers = [...new Set(speakers)];
      }
      state.speakers = speakers;
    },

    addSpeakerIfRequired (state, speakerName: string) {
      const speakers = state.speakers ? [...state.speakers] : [];
      speakers.push(speakerName);
      this.commit('setSpeakers', speakers);
    },

    setLoadedStorageEntryId (state, loadedStorageEntryId: number) {
      state.loadedStorageEntryId = loadedStorageEntryId;
    },

    setEmail (state, email: string) {
      state.email = email;
    },

    changeActiveSectionSpeaker (state, speakerName: string) {
      if (!state.speakers.includes(speakerName)) { //apsisaugom nuo nesąmonių
        return;
      }
      const activeSection = state.sections.find(section => section.id === state.selectedSectionId);
      if (!activeSection) {
        return;
      }
      activeSection.speaker = speakerName
    },

    setDeletedSection(state, deletedSection: DeletedSectionInterface) {
      state.deletedSection = deletedSection;
    },

    setInsertedSectionId(state, sectionId: number) {
      state.insertedSectionId = sectionId
    },

    switchSpeakerInAllSections(state, {from, to}:{from: string, to: string}) {
      state.sections.map(section => {
        if (section.speaker == from.trim()) {
          section.speaker = to.trim();
        }
      })
    },

    setModalVisibility(state, visible: boolean) {
      state.modalVisible = visible;
    },

    setMediaIsPlaying(state, mediaPlayingState: boolean) {
      state.mediaIsPlaying = mediaPlayingState;
    },

    setPlayingMediaStart(state, playingMediaStart: number) {
        state.playingMediaStart = playingMediaStart;
    },

    setPlayingMediaEnd(state, playingMediaEnd: number) {
        state.playingMediaEnd = playingMediaEnd;
    },

    needToSelectPlayingSegments(state, needToSelectPlayingSegments: boolean) {
        state.needToSelectPlayingSegments = needToSelectPlayingSegments;
    },

    modifyCurrentTimeBy(state, seconds: number) {
      if (!state.media) {
        return;
      }
      let newTime = state.media.currentTime + seconds;
      if (newTime < 0) {
        newTime = 0;
      }

      const newSection = state.sections.find(section => section.start <= newTime && section.end > newTime);
      if (!newSection) {
        state.sections.map(section => console.log(section.start, section.end));
        return;
      }
      const totalWordsInNewSection = newSection.words.length;
      let newWordId = newSection.words.findIndex(word => word.end >= newTime);
      if (newWordId === -1) {
        newWordId = totalWordsInNewSection - 1;
      }
      const newWord = newSection.words[newWordId];
      const currentlySelectedWordsCount = state.selectedWordIds.length || 1;

      if (newSection.id !== state.selectedSectionId) {
        this.commit("setSelectedWordIds", null); //kadangi aktyvi sekcija pasikeite, visi zodziai nunulinami
      }

      let selectedWordIds = state.selectedWordIds || [];
      if (!state.selectedWordIds.includes(newWordId)) {
        new SelectService().removeSelection();
        selectedWordIds = [newWordId];
        let i = 1;
        let currentWordId = newWordId + 1
        while (i++ < currentlySelectedWordsCount && currentWordId++ < totalWordsInNewSection) {
          selectedWordIds.push(currentWordId);
        }
      }

      this.commit("setSelectedWordIds", selectedWordIds);
      this.commit("setSelectedSectionId", newSection.id);
      state.media.currentTime = newWord.start;
      this.commit("updateCurrentTime");
    },

    setExtendPlayingRange(state, extendPlayingRange: boolean) {
      state.extendPlayingRange = extendPlayingRange;
    },

    setScrollIntoViewEnabled(state, scrollIntoViewEnabled: boolean) {
      state.scrollIntoViewEnabled = scrollIntoViewEnabled;
    },
  },

  actions: {
    cleanTextData ({commit, state}) {
      commit('setTranscriptionLength', null);
      commit('setSelectedSectionId', null);
      commit('setSelectedWordIds', null);
      commit('setUndoQueue', null);
      commit('setRedoQueue', null);
      commit('rememberCaretPosition', null);
      commit('setSpeakers', null);
      commit('updateFileName', null);
      commit('setSections', null);
      if (state.media) {
        state.media.currentTime = null;
      }
      commit('updateCurrentTime');
    },

    cleanMediaData ({commit}) {
      commit('setMedia', null)
      commit('setMediaLength', null)
      commit('setMediaObjectUrl', null)
      commit('setLoadedStorageEntryId', null)
    },

    cleanActiveSelections ({commit}) {
      commit('setSelectedSectionId', null);
      commit('setSelectedWordIds', null);
    },

    mergeWithNextSection({state, commit, getters, dispatch}) {
      if (state.selectedSectionId === null) {
        return;
      }
      //išsiaiškinam koks yra tolesnes sekcijos identifikatorius masyve
      const nextSectionKey = state.sections.findIndex(section => section.id === state.selectedSectionId + 1);
      if (nextSectionKey < 0) {
        return;
      }
      //iš state išsikerpam tolesnę sekciją
      const nextSection = state.sections.splice(nextSectionKey, 1).shift();
      commit('setDeletedSection', {section: nextSection, key: nextSectionKey});
      //pasiimam tolesnės sekcijos žodžius
      const nextSectionWords = nextSection.words;

      //jungiam tolesnes sekcijos žodžius prie esamos sekcijos
      const selectedSectionWords = getters.selectedSectionWords();
      commit('replaceActiveSectionWords', selectedSectionWords.concat(nextSectionWords));

      dispatch('deleteEmptySectionsAndOrderSectionIds');
    },

    mergeWithPreviousSection({state, commit, getters, dispatch}) {
      if (state.selectedSectionId === null) {
        return;
      }

      //išsiaiškinam koks yra jungiamosios sekcijos identifikatorius masyve
      const connectedSectionKey = state.sections.findIndex(section => section.id === state.selectedSectionId);
      if (connectedSectionKey < 0) {
        return;
      }
      //iš state išsikerpam jungiamą sekciją
      const connectedSection = state.sections.splice(connectedSectionKey, 1).shift();
      commit('setDeletedSection', {section: connectedSection, key: state.selectedSectionId});
      //pakeičiam aktyvią sekciją
      commit('setSelectedSectionId', state.selectedSectionId -1);
      //pasiimam jungiamos sekcijos žodžius
      const connectedSectionWords = connectedSection.words;

      //jungiam jungiamosios sekcijos žodžius prie pagrindinės sekcijos žodžių
      const mainSectionWords = getters.selectedSectionWords();

      commit('replaceActiveSectionWords', mainSectionWords.concat(connectedSectionWords));

      dispatch('deleteEmptySectionsAndOrderSectionIds');
    },

    splitSection({state, commit, dispatch, getters}) {
      if (!state.selectedWordIds || !state.selectedWordIds.length || state.selectedSectionId === null) {
        return;
      }

      // check if need to split a word start
      const carretPosition = getters.caretPosition.start;
      const {wordLength, offset} = carretPosition;
      let startWord, endWord;
      if (offset && wordLength !== offset) {
        //kursorius yra žodyje, turim jį dalinti
        startWord = getters.selectedWords.shift();
        endWord = clone(startWord);
        startWord.content = startWord.content.substring(0, offset);

        const totalWordSpellLength = startWord.end - startWord.start;
        const startSpellLength = Math.round(totalWordSpellLength * offset / wordLength * 100) / 100;
        const wordSplitTime = Math.round((startWord.start + startSpellLength) * 100) / 100;

        startWord.end = wordSplitTime;
        startWord.separator = null;

        endWord.start = wordSplitTime;
        endWord.content = endWord.content.substring(offset, endWord.content.length);
      }

      const splicedSectionKey = state.sections.findIndex(section => section.id === state.selectedSectionId);
      const newSection = clone(state.sections[splicedSectionKey]); //pasidarom naują sekciją kopijuodami aktyvią
      //išmetam nereikalingus žodžius
      const splicedWords = newSection.words.splice(0, state.selectedWordIds.shift()); //splice grąžina iškirptus žodžius
      //būsenoj išsaugom informaciją apie įterptą sekciją, reikalingą undo operacijos sukūrimui
      commit('setInsertedSectionId', splicedSectionKey + 1);
      //jei reikia pridedam nukirpta zodzio dali
      startWord && splicedWords.push(startWord);

      //submitinam aktyvios sekcijos žodžius per metodą, kuris kuria undo eilę
      commit('replaceActiveSectionWords', splicedWords);

      //jei reikia, pirma naujos sekcijos zodi pakeiciam i nukirpto zodzio pabaiga
      if (endWord) {
        newSection.words.splice(0, 1, endWord);
        newSection.start = endWord.start;
      }
      //pridedam naują sekciją į būsenos sekcijų masyvą
      state.sections.splice(splicedSectionKey + 1, 0, newSection);
      //perrikiuojam sekcijų id
      dispatch('deleteEmptySectionsAndOrderSectionIds');
    },

    deleteEmptySectionsAndOrderSectionIds({state, commit}) {
      const nonEmptySections = state.sections.filter(section => section.words.length);
      //pradedam indeksuoti nuo vieneto
      let sectionIndex = 1;
      for (const section of nonEmptySections) {
          section.id = sectionIndex ++;
      }
      commit('setSections', nonEmptySections)
    },

    /**
     * pažymimi visi nurodytos sekcijos žodžiai
     * @param state
     * @param {number} selectedSectionId
     */
    selectAllSectionWords ({state, commit, dispatch, getters}, sectionId) {
      if (!sectionId) {
        return;
      }
      const sectionWords = getters.selectedSectionWords(sectionId)?.map(word => word.id);
      if (!sectionWords.length) {
        return;
      }
      commit('setSelectedSectionId', sectionId);
      commit('setSelectedWordIds', sectionWords.map((word, i) => i));
    },
    renewMediaCurrentTimeBySelectedWords ({state, getters, commit}) {
      if (!state.media || !state.media.paused) {
         return;
      }
      const selectedWords = getters.selectedWords;
      if (selectedWords) {
        state.media.currentTime = selectedWords[0].start;
        commit('updateCurrentTime');
      }
    },
  },
  modules: {

  }
});