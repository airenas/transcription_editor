import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import Notifications from 'vue-notification'
import VueSlideoutPanel from 'vue2-slideout-panel';
import Clipboard from 'v-clipboard';
import Paginate from 'vuejs-paginate'
import HotKeyListener from "@/components/HotKeyListener.vue";
import VueConfirmDialog from "vue-confirm-dialog";
import TitleComponent from "@/components/TitleComponent.vue";
import Modal from "@/components/Modal.vue";
import VModal from 'vue-js-modal';
// @ts-ignore
import i18n from './i18n';
import 'vue-js-modal/dist/styles.css';

Vue.config.productionTip = false;

Vue.use(Notifications);
Vue.use(VueSlideoutPanel);
Vue.use(Clipboard);
Vue.use(VueConfirmDialog);

Vue.component('paginate', Paginate);
Vue.component('HotKeyListener', HotKeyListener);
Vue.component('TitleComponent', TitleComponent);
Vue.component('Modal', Modal);
Vue.use(VModal)

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App)
}).$mount('#app');
