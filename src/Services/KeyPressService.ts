import SelectService from "@/Services/SelectService";

export default class KeyPressService {
  keyPressed:string = null;
  shiftKey: boolean = null;
  altKey: boolean = null;
  ctrlKey: boolean = null;
  // supportedKeys = ["ArrowUp", "ArrowDown"];
  supportedKeys = ["Tab"];

  constructor(e?: KeyboardEvent) {
    if (!e) {
      return;
    }
    this.keyPressed = e.key;
    this.shiftKey = e.shiftKey;
    this.altKey = e.altKey;
    this.ctrlKey = e.ctrlKey;
  }

  /**
   * Ar yra paspaustas navigacinis mygtukas
   */
  navigationKeyPressed = (): Boolean => {
    return this.supportedKeys.includes(this.keyPressed);
  };

  /**
   * reaguojama į navigacijos ar funkcijų klavišus
   * @param selectionService
   */
  respondToNavigationKeyPress = (selectionService: SelectService) => {
    if (!this.navigationKeyPressed()) {
      return;
    }
    if (this.keyPressed === "Tab") {
      selectionService.moveCursorPosition(0, 1);
    }
  };
}