import {SectionInterface} from '@/Interfaces/SectionInterface';
import LineTypeDetectorService, {LineType} from './LineTypeDetectorService';
import SectionService from './SectionService';
import WordService from './WordService';
import {WordAttributesInterface, WordInterface} from "@/Interfaces/WordInterface";
import i18n from '../i18n';

export default class TranscriptionParseService {
  text: string;
  file: File;
  sections: SectionInterface[];
  transcriptionLength: number;

  parseErrors: {
    line: number,
    message: string,
    content: string
  }[] = [];

  sectionService = new SectionService();
  wordService = new WordService();

  constructor(file: File | string) {
    if (typeof file === "string") {
      this.text = file
    } else if (typeof file === "object") {
      this.file = file;
    }
  }

  /**
   * Gaunama įkeltos transkripcijos trukmė
   * 
   * @returns Promise<number>
   */
  async getTranscriptionLength():Promise<number> {
    try {
      if (!this.transcriptionLength) {
        await this.parseTranscription();
      }
      return this.transcriptionLength;
    } catch (e) {
      console.error(e);
      return 0;
    }
  }

  /**
   * Gaunamas įkeltos transkripcijos masyvas
   * 
   * @returns Promise<WordInterface[][]>
   */
  async getSections() {
    if (!this.sections) {
      await this.parseTranscription();
    }
    return this.sections;
  }

  /**
   * Analizuojama įkelta transkripcija
   */
  async parseTranscription() {
    try {
      let content: string = '';
      if (this.file) {
        content = await this.readFileText(this.file);
      } else if (this.text) {
        content = this.text;
      }
      if (!content) {
        return;
      }
      //keičiam &nbsp simbolius tarpais išskaidom tekstą eilutėmis
      const contArr = content.replaceAll(/\u00a0/g, " ").split('\n');
      this.sections = this.generateSectionsArray(contArr);
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Sukuriamas sekcijų masyvas pagal žodžius
   * @param {string[]} contentArray
   * @returns WordInterface[][]
   */
  generateSectionsArray(contentArray: string[]): SectionInterface[] {
    let tmpWordAr: WordAttributesInterface[] = []; //žodžio atributų masyvas
    let wordsArr: WordInterface[] = [];  //sekcijos žodžių masyvas
    let sections:SectionInterface[] = [];
    let currentSection:SectionInterface = null;
    for (let i = 0; i < contentArray.length; i++) {
      const line = contentArray[i].trim();
      if (line === '') {
        continue; //tuščias eilutes praleidžiam
      }
      const lineType = LineTypeDetectorService.detectLineType(line);

      if (lineType === LineType.section) {
        if (currentSection) {
          currentSection.end = tmpWordAr.length ? tmpWordAr[tmpWordAr.length - 1].end : null;
          const wordObj = this.wordService.generateWordObject(tmpWordAr);
          wordObj && wordsArr.push(wordObj);
          currentSection.words = wordsArr;
          tmpWordAr = [];
          wordsArr = [];
          sections.push(currentSection)
        }
        currentSection = this.sectionService.initializeNew(line, sections);
        continue;
      } else if (lineType === undefined) {
        const parseError = {
          line: i + 1,
          message: i18n.t("import.wrong_format").toString(),
          content: line
        }
        this.parseErrors.push(parseError);
        continue;
      }

      if (!currentSection) {
        currentSection = this.sectionService.initializeNew(null, sections);
      }

      const lineAttrs = this.wordService.getAttrs(line);
      // skipping all alternatives
      if (lineAttrs.auto == 0) {
        continue;
      }

      //tikrinam ar tmpWordAr tuščias
      if (!tmpWordAr.length) {
        //patikrinam, ar sekcijai reikalinga pradžia
        if (currentSection.start === null) {
          currentSection.start = lineAttrs.start;
        }
          //Pirmasis žodis. Reikia pildyti žodžio atributų masyvą
          tmpWordAr.push(lineAttrs);
      } else {
        // Kitas žodis. Flushinamas ir naujai pildomas tmpWordAr masyvas
        const wordObj = this.wordService.generateWordObject(tmpWordAr);
        wordObj && wordsArr.push(wordObj);
        tmpWordAr = [];
        tmpWordAr.push(lineAttrs);
      }
    }
    this.transcriptionLength = tmpWordAr[tmpWordAr.length - 1].end;
    const wordObj = this.wordService.generateWordObject(tmpWordAr);
    wordObj && wordsArr.push(wordObj);
    currentSection.end = tmpWordAr.length ? tmpWordAr[tmpWordAr.length - 1].end : null;
    currentSection.words = wordsArr;
    sections.push(currentSection);

    return sections;
  }

  /**
   * Nuskaitomas failas, grąžinamas contentas
   * @param {File} file
   * @returns Prmomise<string>
   */
  async readFileText(file: File): Promise<string> {
    const reader = new FileReader();
    return new Promise((resolve, reject) => {
      reader.onload = (event: any) => resolve(event.target.result);
      reader.onerror = error => reject(error);
      reader.readAsText(file, 'utf-8');
    })
  }
}