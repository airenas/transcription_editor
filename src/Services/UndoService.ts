import {WordInterface} from "@/Interfaces/WordInterface";
import {
  selectedSectionWords,
  selectedSectionId,
  undoQueue,
  sections,
  caretPosition,
  redoQueue, deletedSection, insertedSectionId
} from './helpers/VuexGetters';
import SelectService from "@/Services/SelectService";
import {DeletedSectionInterface} from "@/Interfaces/SectionInterface";
import {setDeletedSection, setInsertedSectionId} from "@/Services/helpers/VuexMutations";
const clone = require('rfdc')();

export enum ChangeType {
  section = 'section',
  word = 'word',
  separator = 'separator'
}

export default class UndoService {

  wordId: number = null;
  sectionId: number = null;
  sectionWords: WordInterface[] = null;
  changeType: ChangeType = null;
  selection: SelectService = null;
  deletedSection: DeletedSectionInterface = null;
  insertedSectionKey: number

  constructor(modifiedWords: WordInterface[], undoService?: UndoService) {
    if (undoService) {
      this.sectionId = undoService.sectionId;
      this.selection = undoService.selection;
    } else {
      this.sectionId = selectedSectionId();
      const selection = new SelectService();
      this.selection = selection.start ? selection : caretPosition() //jeigu naudojama ctrl+x, focusNode ir anchorNode negauna reikšmių, todėl imam paskutinę selection reikšmę iš storage
    }
    this.defineChangeType(modifiedWords);
    this.defineDeletedAndInsertedSection();
  }

/**
* jeigu buvo trinta sekcija, išsaugom ją kintamajame deletedSection ir trinam duomenis iš busenos
*/
  defineDeletedAndInsertedSection() {
    this.deletedSection = clone(deletedSection());
    this.insertedSectionKey = insertedSectionId();
    //jei reikia, apnulinam reikšmes
    this.deletedSection && setDeletedSection(null);
    this.insertedSectionKey && setInsertedSectionId(null);
  }

  /**
   *
   * @param modifiedWords
   */
  defineChangeType = (modifiedWords: WordInterface[]) => {
    if (!modifiedWords) {
      return;
    }
    if (this.changeType) {
      return this.changeType;
    }
    this.sectionWords =  selectedSectionWords(this.sectionId);
    if (!this.sectionWords) {
      throw new Error('no active section words')
    }
    if (!this.sectionWords || modifiedWords.length !== this.sectionWords.length) {
      this.changeType = ChangeType.section;
    } else {
      for (let i = 0; i < this.sectionWords.length; i++) {
        modifiedWords[i].separator = modifiedWords[i].separator === "" ? null : modifiedWords[i].separator;
        this.sectionWords[i].separator = this.sectionWords[i].separator === "" ? null : this.sectionWords[i].separator;

        if (modifiedWords[i].content !== this.sectionWords[i].content) {
          this.markChangedWord(i);
          this.changeType = ChangeType.word;
          return;
        } else if (modifiedWords[i].separator !== this.sectionWords[i].separator) {
          this.markChangedWord(i);
          this.changeType = ChangeType.separator;
          return;
        }
      }
    }
  };

  protected markChangedWord = (wordId: number) => {
    this.wordId = wordId;
    this.sectionWords = [this.sectionWords[wordId]];
  };

  /**
   * gaunami visi sekcijos žodžiai undo operacijai
   * @returns WordInterface[]
   */
  getSectionWordsForUndo = (): WordInterface[] => {
    if (!this.changeType === null) {
      return null;
    } else if (this.wordId !== null && this.sectionWords.length === 1) {
      const sectionWords = selectedSectionWords(this.sectionId);
      sectionWords.splice(this.wordId, 1, this.sectionWords.shift());
      this.sectionWords = sectionWords;
    }

    return this.sectionWords;
  };

  /**
  * generuojamas undo operaciu sarasas pridedant dabartini pokyti
   * @param {WordInterface[]}, words
   * @param 'undo' | 'redo' operation - jei nurodoma 'undo', tuomet išsaugomi dabartiniai busenos žodžiai, jei redo - words masyvo žodžiai
  */
  static getUpdatedUndoQueue = (userModifiedSectionWords: WordInterface[], undoOperation?: UndoService) : UndoService[] => {
    const undoService = new UndoService(userModifiedSectionWords, undoOperation);
    if (undoService.changeType === null) {
      return undoQueue();
    }
    let queue = undoQueue() ? [undoService].concat(undoQueue()) : [undoService];

    return UndoService.removeExcessFromQueue(queue);
  };

  /**
   * generuojamas redo operacijų sąrašas
   * @param undoOperation
   */
  static getUpdatedRedoQueue = (words: WordInterface[], undoOperation: UndoService): UndoService[] => {
    const redoService = new UndoService(words, undoOperation);
    if (redoService.changeType === null) {
      return redoQueue();
    }
    let queue = redoQueue() ? [redoService].concat(redoQueue()) : [redoService];

    return UndoService.removeExcessFromQueue(queue);
  };

  /**
   * Pašalinamos perteklinės undo ir redo operacijos pagal nurodytą limitą
   * @param queue
   */
  static removeExcessFromQueue = (queue: UndoService[]): UndoService[] => {
    const allowedUndoOpsCount = process.env.VUE_APP_ALLOWED_UNDO_OPS_COUNT ? parseInt(process.env.VUE_APP_ALLOWED_UNDO_OPS_COUNT) : 100;
    //pašalinam perteklines operacijas
    const delCount = queue.length - allowedUndoOpsCount;
    delCount > 0 && queue.splice(allowedUndoOpsCount, delCount);
    return queue;
  }
}