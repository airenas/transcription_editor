import {
  rememberCaretPosition,
  setSelectedSectionId,
  setSelectedWordIds
} from "@/Services/helpers/VuexMutations";

export interface SelectInterface {
  sectionId: number,
  wordId: number,
  offset: number,
  wordLength: number
}
export default class SelectService {
  start: SelectInterface = null;
  end: SelectInterface = null;
  range: boolean = null;
  selection: Selection = window.getSelection();

  SECTION_CHANGE_TO_SAME_OFFSET = true;

  constructor() {
    this.parseSelection();
  };

  /**
   * Gaunami pažymėti žodžiai
   */
  parseSelection = () => {
    if (this.sectionSelected()) {
      return;
    }
    const anchorPosition = this.getAnchorPosition();
    const focusPosition = this.getFocusPosition();

    this.range = !anchorPosition || !focusPosition ||
      anchorPosition.wordId !== focusPosition.wordId ||
      anchorPosition.offset !== focusPosition.offset;

    if (!anchorPosition && !focusPosition) {
      this.start = null;
      this.end = null;

      return;
    }

    if (!anchorPosition || !focusPosition) {
      this.start = anchorPosition ? anchorPosition : focusPosition;
      this.end = null;
    } else {
      //pirmiausia tikrinam pagal wordId, paskui jei wordid suptampa pagal offset
      if (anchorPosition.wordId > focusPosition.wordId) {
        this.start = focusPosition;
        this.end = anchorPosition;
      } else if (anchorPosition.wordId < focusPosition.wordId) {
        this.start = anchorPosition;
        this.end = focusPosition;
      } else if (anchorPosition.wordId === focusPosition.wordId) {
        if (anchorPosition.offset === focusPosition.offset) {
          this.start = anchorPosition;
          this.end = focusPosition;
        } else if (anchorPosition.offset < focusPosition.offset) {
          this.start = anchorPosition;
          this.end = focusPosition;
        } else if (anchorPosition.offset > focusPosition.offset) {
          this.start = focusPosition;
          this.end = anchorPosition;
        }
      }
    }
  };

/**
* ar pažymėta sekcija?
*/
  sectionSelected = (): boolean => {
    if (!this.selection || !this.selection.focusNode) {
      return false;
    }
    const element = this.selection.focusNode.parentElement;
    if (element.className === 'section-margin') { //SVARBU!!!! className turi atitikti src/components/SpeakerComponent.vue templeito elemento klase
      const sectionId = this.getSectionId(element);
      if (sectionId === null) {
        return false;
      }
      const sectionWords = this.getSectionWords(sectionId);
      this.start = {
        sectionId,
        wordId: parseInt(sectionWords[0].dataset.wordId),
        offset: null,
        wordLength: null
      };
      this.end = {
        sectionId,
        wordId: parseInt(sectionWords[sectionWords.length - 1].dataset.wordId),
        offset: null,
        wordLength: null
      };
      return true;
    }
    return false;
  };

  getSelectedWordIds = (): number[] => {
    let selectedWordIds = [];
    if ((!this.start && !this.end) || (this.start.wordId === null && this.end.wordId === null)) {
      return selectedWordIds;
    } else if (!this.start || !this.end) {
      this.end = this.end ? this.end : this.start;
      this.start = this.start ? this.start : this.end;
    }
    for (let i = this.start.wordId; i <= this.end.wordId; i++) {
      selectedWordIds.push(i);
    }
    return selectedWordIds;
  };

  /**
   * gaunama žymos pradžia
   */
  getAnchorPosition = () :SelectInterface => {
    if (!this.selection || !this.selection.anchorNode) {
      return null;
    }

    const element = this.selection.anchorNode.parentElement;
    const anchorNode = this.getWordElement(element, 'anchor');
    const anchorOffset = this.getWordElementOffset(element, 'anchor');
    return this.returnSelectPosition(anchorNode, anchorOffset);
  };

  /**
   * gaunama žymos pabaiga
   */
  getFocusPosition = (): SelectInterface => {
    if (!this.selection || !this.selection.focusNode) {
      return null;
    }
    const element = this.selection.focusNode.parentElement;
    const focusNode = this.getWordElement(element, 'focus');
    const focusOffset = this.getWordElementOffset(element, 'focus');
    return this.returnSelectPosition(focusNode, focusOffset);
  };

  /**
   * gaunamas zodzio elementas pagal kursoriaus padeti
   * @param element
   * @param type
   */
  getWordElement = (element: HTMLElement, type: 'focus' | 'anchor'): HTMLElement => {
    if (!element) {
      return null;
    }
    let wordElement: HTMLElement = null;

    if (element.className.includes('word')) {
      wordElement = element;
    } else if (element.className === 'separator') {
      const offset = this.getOffsetByType(type);
      wordElement = offset === 0 ? <HTMLElement>element.previousElementSibling : <HTMLElement>element.nextElementSibling;
    }
    return wordElement;
  };

  /**
   * gaunama sekcija, kurioje yra pažymėtas elementas
   * @param element
   */
  getSectionId (element: HTMLElement): number {
    const sectionElement = <HTMLElement>element.closest('.section');
    let sectionId = null;
    if (sectionElement) {
      const dataset = sectionElement.dataset;
      if (dataset && typeof dataset.key !== 'undefined') {
        sectionId = parseInt(dataset.key);
      }
    }
    sectionId === null && console.error('undefined section id!!!!!')
    return sectionId;
  }

  /**
   * grąžina žymos pradžios arba pabaigos poziciją. Jeigu žyma ant separatoriaus, ji yra perkeliama
   * @param element
   * @param type
   */
  getWordElementOffset = (element: HTMLElement, type: 'focus' | 'anchor'): number => {
    let offset = this.getOffsetByType(type);

    if (element.className === 'separator') {
      if (offset > 0) {
          offset = 0;
      } else if (offset === 0) {
        offset = element.previousElementSibling.textContent.length;
      }
    }
    return offset;
  };

  /**
   * paduodamas offsetas paselectintam html elemente pagal tipą (selecto pradžia ar pabaiga)
   * @param type
   */
  getOffsetByType = (type: 'focus' | 'anchor') => {
    return type === 'focus' ? this.selection.focusOffset : this.selection.anchorOffset
  }

  /**
   * Suformuoja select pozicijos atsakymą, kad atitiktų SelectInterface
   * @param selectionNode
   * @param offset
   */
  returnSelectPosition = (selectionNode:HTMLElement, offset: number): SelectInterface => {
    if (!selectionNode) {
      return null;
    }
    const wordId = parseInt(selectionNode.dataset.wordId.split('_').slice(-1).shift());
    const sectionId = parseInt(selectionNode.dataset.wordId.split('_').shift());
    if (isNaN(wordId) || isNaN(offset) || isNaN(sectionId)) {
      return null;
    }
    return {
      sectionId,
      wordId,
      offset,
      wordLength: selectionNode.innerText.length
    }
  };

  /**
   * pastumia kursoriaus poziciją per reikalingą x ir y padėčių skaičių
   * @param x
   * @param y
   */
  moveCursorPosition = (x?: number, y?: number) => {
    if (!this.start) {
      console.log('start position undefined');
      return;
    }
    if (!this.getSectionWords(this.start.sectionId).hasOwnProperty(this.start.wordId)) {
      console.log('word undefined');
      return;
    }
    this.moveCursorXPosition(x);
    this.moveCursorYPosition(y);
  };

  /**
   * Skaičiavimai, reikalingi kursoriaus poziciją pastumti horizontaliai
   * @param x
   */
  moveCursorXPosition = (x: number) => {
    if (null === x) {
      return;
    }
    const allWords = this.getSectionWords(this.start.sectionId);

    let selectedWord = allWords[this.start.wordId];
    if (!selectedWord) {
      return;
    }
    let offset = this.start.offset + x;

    if (selectedWord.innerText.length < x + this.start.offset) { //reikia šokti prie tolesnio žodžio
      if (allWords.hasOwnProperty(this.start.wordId + 1)) { //yra sekantis žodis
        selectedWord = allWords[this.start.wordId + 1];
        offset = 0;
      } else { //sekančio žodžio nėra
        offset = selectedWord.innerText.length;
      }
    } else if (x + this.start.offset < 0) { // reikia šokti prie ankstesnio žodžio
      if (allWords.hasOwnProperty(this.start.wordId - 1)) { //yra ankstesnis žodis
        selectedWord = allWords[this.start.wordId - 1];
        offset = selectedWord.innerText.length;
      } else { //nėra ankstesnio žodžio
        offset = 0;
      }
    }
    this.moveCaret(selectedWord, offset)
  };

  /**
   * Kursorius perkeliamas į nurodytą elemento vietą
   * @param {HTMLElement} element
   * @param {number} offset
   */
  moveCaret = (element: HTMLElement, offset: number) => {
    if (!element || !element.childNodes) {
      return;
    }
    const range = document.createRange();
    range.setStart(element.childNodes[0], offset);
    range.collapse(true);
    this.selection.removeAllRanges();
    this.selection.addRange(range);
  };

  moveCursorToStartOfNextSection = () => {
    const nextSectionWords = this.getSectionWords(this.start.sectionId + 1);
    if (!nextSectionWords.length) {
      return;
    }
    this.moveCaret(nextSectionWords[0], 0);
  }

  moveCursorToEndOfPreviousSection = () => {
    const previousSectionWords = this.getSectionWords(this.start.sectionId -1);
    if (!previousSectionWords.length) {
      return;
    }
    const lastWordOfPreviousSection = previousSectionWords[previousSectionWords.length - 1];
    this.moveCaret(lastWordOfPreviousSection, lastWordOfPreviousSection.innerText.length);
  }

  /**
   * Skaičiavimai, reikalingi kursoriaus poziciją pastumti vertikaliai
   * @param y
   */
  moveCursorYPosition = (y: number) => {
    if (!y) {
      return;
    }
    const allSections = this.getAllSectionsElements();
    let activeWord = null;
    let offset = 0;
    const nextSection = allSections.find(section => section.dataset.hasOwnProperty('key') && parseInt(section.dataset.key) === this.start.sectionId + y);
    if (nextSection) {
      const activeSectionWords = this.getSectionWords(this.start.sectionId + y);
      if (!activeSectionWords) {
        return;
      }
      if (this.SECTION_CHANGE_TO_SAME_OFFSET) {
        const currentOffset = this.currentOffsetFromSectionStart();
        const neededSectionOffset = this.getSectionWordAndOffsetToMeetTextOffset(this.start.sectionId + y, currentOffset);
        activeWord = neededSectionOffset.word;
        offset = neededSectionOffset.wordOffset;
      } else {
        activeWord = activeSectionWords[0];
      }
    } else {
      const activeSectionWords = this.getSectionWords(this.start.sectionId);
      if (!activeSectionWords) {
        return;
      }
      activeWord = y > 0 ? activeSectionWords[activeSectionWords.length - 1] : activeSectionWords[0];
      offset = y > 0 ? activeWord.innerText.length : 0;
    }
    if (!activeWord) {
      return;
    }
    this.moveCaret(activeWord, offset);
  };

  /**
   * Gaunami visi nurodytos sekcijos .word elementai
   * @param sectionId
   */
  getSectionWords = (sectionId: number): HTMLElement[] => {
    if (sectionId === null) {
      return [];
    }
    let activeSection = document.querySelectorAll(".section[data-key='" + sectionId + "']").item(0);
    // const activeSection = this.getAllSectionsElements().find(section => parseInt(section.dataset.key) === sectionId);
    if (!activeSection) {
      return [];
    }
    const activeSectionChildren = Array.prototype.slice.call(activeSection.childNodes);
    if (!activeSectionChildren) {
      return [];
    }
    const activeSectionWordsWithSeparatorsParentElement = activeSectionChildren.find(child => child.className === 'words');
    if (!activeSectionWordsWithSeparatorsParentElement) {
      return [];
    }
    const activeSectionWordsWithSeparatorsArray = Array.prototype.slice.call(activeSectionWordsWithSeparatorsParentElement.children);
    const wordsArray = activeSectionWordsWithSeparatorsArray.filter(element => element.className.includes('word'));
    return wordsArray;
  };

  /**
   * gaunami visi .section elementai
   */
  getAllSectionsElements = (): HTMLElement[] => {
    const transcription = document.getElementById("right-pane").children;
    return Array.prototype.slice.call(transcription).filter(element => element.className === 'section');
  };

  /**
   * pagal vidinius klasės duomenis atstatoma žyma
   */
  resetSelection = () => {
    if (!this.start || !this.end) {
      return;
    }
    this.saveSelectedWordsAndSectionToState();

    const allWords = this.getSectionWords(this.start.sectionId);

    if (!allWords.hasOwnProperty(this.start.wordId) || !allWords.hasOwnProperty(this.end.wordId)) {
      return;
    }

    let startWord = allWords[this.start.wordId];
    let startOffset = this.start.offset;
    let endWord = allWords[this.end.wordId];
    const endWordLen = endWord.innerText.length;
    let endOffset = this.end.offset;

    if (!startWord || !endWord || startOffset === null || endOffset === null) {
      return;
    }

    const range = document.createRange();
    range.setStart(startWord.childNodes[0], startOffset);
    range.setEnd(endWord.childNodes[0], endOffset > endWordLen ? endWordLen : endOffset);

    this.selection.removeAllRanges();
    this.selection.addRange(range);
  };

  /**
   * pažymimi visi nurodytos sekcijos žodžiai
   */
  selectAllSectionWords = (sectionId: number) => {
    this.saveSelectedWordsAndSectionToState();

    const allWords = this.getSectionWords(sectionId);
    let startWord = allWords[0];
    let startOffset = 0;
    let endWord = allWords[allWords.length - 1];
    const endWordLen = endWord.innerText.length;

    if (!startWord || !endWord || startOffset === null) {
      return;
    }

    const range = document.createRange();
    range.setStart(startWord.childNodes[0], startOffset);
    range.setEnd(endWord.childNodes[0], endWordLen);

    this.selection.removeAllRanges();
    this.selection.addRange(range);
  };

  /**
   * Žyma yra pašalinama
   */
  removeSelection = (selectionOnly: boolean = false) => {
    this.selection && this.selection.removeAllRanges();
    this.start = null;
    this.end = null;
    this.range = null;
    this.selection = null;
    this.saveSelectedWordsAndSectionToState(selectionOnly);
  };

  /**
   * būsenoj išsaugomi aktyvūs žodžiai ir sekcija
   */
  saveSelectedWordsAndSectionToState = (selectionOnly: boolean = false) => {
    //jei reikia, žyma išsaugoma ir busenoj
    !selectionOnly && setSelectedSectionId(this.start ? this.start.sectionId : null);
    !selectionOnly && setSelectedWordIds(this.getSelectedWordIds());
    rememberCaretPosition(this);
  };

  /**
  * gaunama dabartinė kursoriaus pozicija nuo sekcijos pradžios
   * @returns number
  */
  currentOffsetFromSectionStart = (): number => {
    const words = this.getSectionWords(this.start.sectionId).map(word => word.innerText);
    let offsetFromStart = 0;
    for (let i = 0; i < this.start.wordId; i++ ) {
      offsetFromStart += words[i].length + 1; //priskaičiuojame ir separatorių
    }
    offsetFromStart += this.start.offset;
    return offsetFromStart;
  };

  /**
   * gaunamas aktyvios sekcijos simbolių ilgis
   * @returns number
   */
  allSectionLength = (): number => {
    const words = this.getSectionWords(this.start.sectionId).map(word => word.innerText);
    return this.countWordsLength(words);
  };

  countWordsLength = (words: string[], includeSeparators = true): number => {
    let sectionLength = 0;
    for (let i = 0; i < words.length; i++ ) {
      sectionLength += words[i].length;
      sectionLength = includeSeparators ? sectionLength + 1 : sectionLength;
    }
    sectionLength -= 1;
    return sectionLength;
  }

  /**
  * gaunamas žodžio elementas ir offset reikšmė, atitinkanti nurodytą nuotolį nuo sekcijos pradžios
  */
  getSectionWordAndOffsetToMeetTextOffset = (sectionId: number, offset: number): {word:HTMLElement, wordOffset: number} => {
    const words = this.getSectionWords(sectionId).map(word => word.innerText);
    let offsetFromStart: number = 0;
    let wordId: number = 0;
    let wordOffset: number = 0;
    for (let i = 0; i < words.length; i++ ) {
      if ((offsetFromStart + words[i].length) >= offset) {
        wordId = i;
        wordOffset = offset - offsetFromStart;
        break;
      } else {
        offsetFromStart += words[i].length + 1;
      }
    }
    if (offset > 0 && !wordId && !wordOffset) {
      wordId = words.length - 1;
      wordOffset = words[wordId].length
    }
    return {
      word: this.getSectionWords(sectionId)[wordId],
      wordOffset
    }
  };
}