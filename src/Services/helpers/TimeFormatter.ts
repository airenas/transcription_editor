export const formatSeconds = (seconds: number): string => {
  const date = new Date(0);
  date.setSeconds(seconds);
  const timeString = date.toISOString().substr(11, 8);
  if (timeString.indexOf('00:') === 0) {
    return timeString.slice(3);
  }
  return timeString;
}