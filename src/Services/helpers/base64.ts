/**
 * encode string to base64
 * @param str
 */
export const base64encode = (str: string): string => {
  return btoa(unescape(encodeURIComponent(str)));
}

/**
 * decode string from base64
 * @param str
 */
export const base64decode = (str: string): string => {
  return decodeURIComponent(escape(window.atob(str)));
}
