const GUESSED_SEPARATOR_BEGIN = process.env.VUE_APP_GUESSED_SEPARATOR_BEGIN || "<";
const GUESSED_SEPARATOR_END = process.env.VUE_APP_GUESSED_SEPARATOR_END || ">";
const SPACE_SEPARATOR = process.env.VUE_APP_SPACE_SEPARATOR || "_";
const SILENCE_ELEMENT = process.env.VUE_APP_SILENCE_ELEMENT || "<env>";

export const addSpaceSeparator = (text: string): string => {
  return text.trim().replaceAll(/\u00a0/g, " ").replaceAll(" ", SPACE_SEPARATOR);
};

export const replaceSpaceSeparator = (text: string): string => {
  return text.trim().replaceAll(SPACE_SEPARATOR, " ");
};

export const wordIsGuessed = (text: string): boolean => {
  return text.length > 2 &&
    text !== SILENCE_ELEMENT &&
    text.slice(0, 1) === GUESSED_SEPARATOR_BEGIN &&
    text.slice(-1) === GUESSED_SEPARATOR_END
}

/**
 * nukerpami atspėto žodžio žymėjimai
 * @param text
 */
export const replaceGuessedMarks = (text: string): string => {
    return wordIsGuessed(text) ? text.slice(1, -1) : text;
}
/**
* pridedamos nuspėto žodžio žymos
*/
export const addGuessedMarks = (text: string): string => {
  return GUESSED_SEPARATOR_BEGIN + text + GUESSED_SEPARATOR_END;
}