import {DeletedSectionInterface, SectionInterface} from "@/Interfaces/SectionInterface";
import {WordInterface} from "@/Interfaces/WordInterface";
import store from '../../store';
import SelectService from "@/Services/SelectService";
import {HotKeyCtrlActions} from "@/Services/HotKeyService";
import {mediaLength} from "@/Services/helpers/VuexGetters";

//Vuex mutations
export const updateCurrentTime = (): void => store.commit('updateCurrentTime');
export const setSelectedWordIds = (selectedWordIds: number[]): void => store.commit('setSelectedWordIds', selectedWordIds);
export const setSelectedSectionId = (selectedSectionId: number): void => store.commit('setSelectedSectionId', selectedSectionId);
export const replaceActiveSectionWords = (words: WordInterface[]): void => store.commit('replaceActiveSectionWords', words);
export const updateFileName = ($filename: string): void => store.commit('updateFileName', $filename);
export const setSections = (sections: SectionInterface[]): void => store.commit('setSections', sections);
export const setMediaAndLength = (mediaElement: HTMLMediaElement): void => {
  //set media and media length
  store.commit('setMedia', mediaElement);
  mediaElement.addEventListener('loadeddata', () => {
    setMediaLength(mediaElement.duration);
  })
}
export const setMediaLength = (setMediaLength: number): void => store.commit('setMediaLength', mediaLength);
export const setTranscriptionLength = (transcriptionLength: number): void => store.commit('setTranscriptionLength', transcriptionLength);
export const undoUserAction = (): void => store.commit('undoUserAction');
export const redoUserAction = (): void => store.commit('redoUserAction');
export const rememberCaretPosition = (selectService:SelectService): void => store.commit('rememberCaretPosition', selectService);
export const setSpeakers = (speakers:string[]): void => store.commit('setSpeakers', speakers);
export const addSpeakerIfRequired = (speakerName:string): void => store.commit('addSpeakerIfRequired', speakerName);
export const setHotKey = (hotKey:HotKeyCtrlActions): void => store.commit('setHotKey', hotKey);
export const setLoadedStorageEntryId = (id:number): void => store.commit('setLoadedStorageEntryId', id);
export const setCurrentPage = (currentPage:number): void => store.commit('setCurrentPage', currentPage);
export const setDeletedSection = (deletedSection:DeletedSectionInterface): void => store.commit('setDeletedSection', deletedSection);
export const setInsertedSectionId = (insertedSectionId:number): void => store.commit('setInsertedSectionId', insertedSectionId);