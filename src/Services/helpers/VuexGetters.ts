import {DeletedSectionInterface, SectionInterface} from "@/Interfaces/SectionInterface";
import {WordInterface} from "@/Interfaces/WordInterface";
import store from '../../store';
import UndoService from "@/Services/UndoService";
import SelectService from "@/Services/SelectService";
import {HotKeyCtrlActions} from "@/Services/HotKeyService";
import {formatSeconds} from "@/Services/helpers/TimeFormatter";

//Vuex Getters
export const media = (): HTMLMediaElement => {return store.getters.media};
export const sections = (): SectionInterface[] => store.getters.sections;
export const selectedSectionWords = (sectionId?: number): WordInterface[] => store.getters.selectedSectionWords(sectionId);
export const mediaLength = (): number => store.getters.mediaLength;
export const transcriptionLength = (): number => store.getters.transcriptionLength;
export const primaryWords = (sectionId?: number): string[] => store.getters.primaryWords(sectionId);
export const selectedWords = (): WordInterface[] => store.getters.selectedWords;
export const selectedWordIds = (): number[] => store.getters.selectedWordIds;
export const selectedSectionId = (): number => store.getters.selectedSectionId;
export const fileName = (): string => store.getters.fileName;
export const undoQueue = (): UndoService[] => store.getters.undoQueue;
export const redoQueue = (): UndoService[] => store.getters.redoQueue;
export const recentUndoAction = (): UndoService => store.getters.recentUndoAction;
export const recentRedoAction = (): UndoService => store.getters.recentRedoAction;
export const caretPosition = (): SelectService => store.getters.caretPosition;
export const caretIsAtSectionStart = (): boolean => {
  const caretPosition: SelectService = store.getters.caretPosition;
  return caretPosition.range === false && caretPosition.currentOffsetFromSectionStart() === 0;
}
export const caretIsAtSectionEnd = (): boolean => {
  const caretPosition: SelectService = store.getters.caretPosition;
  return caretPosition.range === false && caretPosition.currentOffsetFromSectionStart() === caretPosition.allSectionLength();
}
export const previousSectionExists = (): boolean => store.getters.selectedSectionId > 0;
export const nextSectionExists = (): boolean => store.getters.selectedSectionId < (store.getters.sections.length - 1);
export const speakers = (): string[] => store.getters.speakers;
export const hotKey = (): HotKeyCtrlActions => store.getters.hotKey;
export const email = (): string => store.getters.email;
export const lattice = (): string => {
  const allSections = store.getters.sections;
  let lattice = '';
  for (const section of allSections) {
    if (lattice !== '') {
      lattice += "\n";
    }
    const speakerInfo = `# ${section.speaker} (${formatSeconds(section.start)})`;

    const text = section.words.map(word => {
      const separator = typeof word.separator === 'string' ? word.separator : '';
      return word.content + separator;
    }).join(' ');
    lattice += speakerInfo + "\n" + text;
  }
  return lattice;
}
export const pageCount = (): number => store.getters.pageCount;
export const perPage = (): number => store.getters.perPage;
export const currentPage = (): number => store.getters.currentPage;
export const deletedSection = (): DeletedSectionInterface => store.getters.deletedSection;
export const insertedSectionId = (): number => store.getters.insertedSectionId;
/**
 * gaunamas pažymėtos teksto atkarpos laiko rėžis
 */
export const currentSelectionTimeLimits = () => {
  let start: number = null;
  let end: number = null;

  if (store.getters.selectedWords && store.getters.selectedWords.length) {
    start = store.getters.selectedWords[0].start;
    end = store.getters.selectedWords[store.getters.selectedWords.length - 1].end;
    if (store.getters.extendPlayingRange) {
      start = start - 1;
      if (start < store.getters.selectedSection.start) {
        start = store.getters.selectedSection.start;
      }
      end = end + 1;
      if (end > store.getters.selectedSection.end) {
        end = store.getters.selectedSection.end;
      }
    }
  }

  //pakoreguojam laiką, jei yra pažymėtas pirmas arba paskutinis sekcijos žodis
  if (store.getters.firstWordSelected && store.getters.selectedSection) {
    start = store.getters.selectedSection.start;
  }

  if (store.getters.lastWordSelected && store.getters.selectedSection) {
    end = store.getters.selectedSection.end;
  }

  return {start, end}
};