import KeyPressService from "@/Services/KeyPressService";
import {undoQueue} from "@/Services/helpers/VuexGetters";

export enum HotKeyCtrlActions {
  z = 'undo',
  p = 'play',
  s = 'save',
  d = 'save-db',
  y = 'redo',
  c = 'copy',
  f = 'ffw',
  b = 'fbw',
  l = 'save-lattice'
}
export enum HotKeyActions {
  Escape = 'Escape',
  Enter  = 'Enter'
}
export enum HotKeyShiftActions {

}
export enum HotKeyAltActions {

}

export default class HotKeyService {
  keyPress: KeyPressService = null;

  constructor(keyPress: KeyPressService) {
    this.keyPress = keyPress;
  };

  /**
   * patikrina, ar hotkey yra supportinamas
   */
  keyIsSupported = ():boolean => {
    return  ( Object.keys(HotKeyCtrlActions).includes(this.keyPress.keyPressed) &&
              this.keyPress.ctrlKey &&
              !this.keyPress.altKey &&
              !this.keyPress.shiftKey
            ) || ( Object.keys(HotKeyShiftActions).includes(this.keyPress.keyPressed) &&
              !this.keyPress.ctrlKey &&
              !this.keyPress.altKey &&
              this.keyPress.shiftKey
            ) || ( Object.keys(HotKeyAltActions).includes(this.keyPress.keyPressed) &&
              !this.keyPress.ctrlKey &&
              this.keyPress.altKey &&
              !this.keyPress.shiftKey
            ) || ( Object.keys(HotKeyActions).includes(this.keyPress.keyPressed) &&
              !this.keyPress.ctrlKey &&
              !this.keyPress.altKey &&
              !this.keyPress.shiftKey
            );
  };

  /**
   * grąžina hotkey actioną
   */
  hotKeyAction = (): HotKeyCtrlActions => {
    if (!this.keyIsSupported()) {
      return null;
    }
    if (!undoQueue() && this.keyPress.keyPressed === "z") {
      return null;
    }
    if (this.keyPress.ctrlKey) {
      return HotKeyCtrlActions[this.keyPress.keyPressed];
    } else if (this.keyPress.altKey) {
      return HotKeyAltActions[this.keyPress.keyPressed];
    } else if (this.keyPress.shiftKey) {
      return HotKeyShiftActions[this.keyPress.keyPressed];
    } else {
      return HotKeyActions[this.keyPress.keyPressed];
    }
  };
}