import store from '../store';
import {WordInterface} from "@/Interfaces/WordInterface";
import {SectionInterface} from "@/Interfaces/SectionInterface";
import {addGuessedMarks, addSpaceSeparator} from "@/Services/helpers/SpaceSeparator";

export default class DataExportService {

  SILENCE_ELEMENT = process.env.VUE_APP_SILENCE_ELEMENT || "<eps>";

  /**
   * generuojamas tekstinis failas pagal esamus būsenos duomenis
   * @returns string
   */
  public generateExportedData = (): string => {
    const sections = store.getters.sections;
    let text = '';
    sections.map((section: SectionInterface) => {
      text += this.getSectionElement(section);    //pridedam sekcijos tekstą
      if (process.env.VUE_APP_ENABLE_SILENCE_EXPORT == '1') {
        text += this.getLeadingSilenceElement(section); //pridedam <eps> elementą prieš pirmą žodį
      }
      text += this.getWordElements(section);      //pridedam žodžius
      if(process.env.VUE_APP_ENABLE_SILENCE_EXPORT == '1') {
        text += this.addTrailingSilenceElement(section); //pridedam <eps> elementą po paskutinio žodžio
      }
    });
    return text.trim();
  }

  /**
   * pateikiama tekstinė eilutė su sekcijos informacija
   * @param section
   * @private
   * @returns string
   */
  private getSectionElement(section: SectionInterface): string {
    let transcriptionText: string = '';
    transcriptionText += `# ${section.id} ${section.speaker}\n`;
    return transcriptionText;
  }

  /**
   * jei yra, pateikiama tekstinė eilutė su tylos informacija sekcijos pradžioje
   * @param section
   * @private
   * @returns string
   */
  private getLeadingSilenceElement (section: SectionInterface): string {
    let transcriptionText: string = '';
    const sectionStartTime = section.start;
    const firstSectionWordStartTime = section.words[0].start;
    if (sectionStartTime !== firstSectionWordStartTime) {
      transcriptionText += `1 ${sectionStartTime} ${firstSectionWordStartTime} ${this.SILENCE_ELEMENT}\n`;
    }
    return transcriptionText;
  }

  /**
   * pateikia sekcijos žodžius
   * @param section
   * @private
   * @returns string
   */
  private getWordElements (section: SectionInterface): string {
    let transcriptionText: string = '';
    section.words.map((word:WordInterface) => {
      const wordStart = word.start;
      const wordEnd = word.end;
      let content = addSpaceSeparator(word.content);
      content = word.guessed && !word.modified ? addGuessedMarks(content) : content;
      const separator = word.separator ? addSpaceSeparator(word.separator) : '';
      transcriptionText += `1 ${wordStart} ${wordEnd} ${content}${separator ? ' ' + separator : ''}\n`;
    })
    return transcriptionText;
  }

  /**
   * jei yra, pateikiama tekstinė eilutė su tylos informacija sekcijos pabaigoje
   * @param section
   * @private
   * @returns string
   */
  private addTrailingSilenceElement (section: SectionInterface): string {
    let transcriptionText: string = '';
    const sectionEndTime = section.end;
    const lastSectionWordEndTime = section.words[section.words.length - 1].end;
    if (sectionEndTime !== lastSectionWordEndTime) {
      transcriptionText += `1 ${lastSectionWordEndTime} ${sectionEndTime} ${this.SILENCE_ELEMENT}\n`;
    }
    return transcriptionText;
  }
}