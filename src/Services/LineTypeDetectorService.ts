export enum LineType {
  section,
  word
}

export default class LineTypeDetectorService {

  /**
   * Nustatomas eilutės tipas
   * @param line 
   * 
   * @returns LineType
   */
    static detectLineType(line: string): LineType {
      let splitedLine = line.split(' ');
      
      if (LineTypeDetectorService.lineIsWord(splitedLine)) {
        return LineType.word
      } else if (LineTypeDetectorService.lineIsSection(splitedLine)) {
        return LineType.section
      }

      return undefined;
    }

    /**
     * Ar eilutė yra žodžio?
     * @param splitedLine 
     */
    static lineIsWord(splitedLine: any): boolean {
      const wordFirstSymbol = ['0', '1'];
      return splitedLine.length >= 4
        && wordFirstSymbol.includes(splitedLine[0])
        && !isNaN(parseFloat(splitedLine[1]))
        && !isNaN(parseFloat(splitedLine[2]))
    }

    /**
     * Ar eilutė yra sekcijos?
     * @param splitedLine 
     */
    static lineIsSection(splitedLine: any): boolean {
      return splitedLine.length >= 3
          && splitedLine[0] == '#'
          && !isNaN(parseFloat(splitedLine[1]))
    }
}