import {SectionInterface} from '@/Interfaces/SectionInterface';
import {speakers} from "@/Services/helpers/VuexGetters";
import {addSpeakerIfRequired} from "@/Services/helpers/VuexMutations";
import {replaceSpaceSeparator} from "@/Services/helpers/SpaceSeparator";

export default class SectionService {

  presetColors = [
    "#C2185B",
    "#AFB42B",
    "#FFF176",
    "#0D47A1",
    "#CFD8DC",
    "#795548",
    "#FBC02D",
    "#512DA8",
    "#E65100",
    "#1B5E20",
    "#03A9F4",
    "#BA68C8"
  ];

  SILENCE_SECTION_SPEAKER_NAME = process.env.VUE_APP_SILENCE_SECTION_SPEAKER_NAME || "TYLA";
  UNKNOWN_SPEAKER_NAME = process.env.VUE_APP_DEFAULT_UNKNOWN_SPEAKER_NAME || "nežinomas"

  /**
   * Inicijuojamas naujos sekcijos kūrimas
   * @param line 
   */
  initializeNew(line: string, sections: SectionInterface[]):SectionInterface {
    let lineArr = line ? line.split(' ') : null;
    const speakerName = lineArr && lineArr.hasOwnProperty(2) ? replaceSpaceSeparator(lineArr[2]) : this.UNKNOWN_SPEAKER_NAME;
    addSpeakerIfRequired(speakerName);

    return {
      id: this.assignSectionId(lineArr, sections),
      speaker: speakerName,
      start: null,
      end: null,
      words: null,
    }
  }

  assignSectionId (lineArr: string[], sections: SectionInterface[]): number {
    let sectionId = 1;
    if (lineArr
        && lineArr.hasOwnProperty(1)
        && !isNaN(parseInt(lineArr[1]))
    ) {
      sectionId = parseInt(lineArr[1]);
    }

    if (sections && sections.length) {
      for (const s of sections) {
        sectionId = s.id >= sectionId ? s.id + 1 : sectionId;
      }
    }

    return sectionId;
  }

  speakerWithNameExists = (speakerName: string): boolean => {
    speakerName = replaceSpaceSeparator(speakerName)
    return speakers().includes(speakerName);
  };

  getRandomColor = (): string => {
    if (this.presetColors.length) {
      return this.presetColors.shift();
    }
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
}