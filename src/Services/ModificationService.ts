import KeyPressService from "@/Services/KeyPressService";
import SelectService from "@/Services/SelectService";
import {WordInterface} from "@/Interfaces/WordInterface";
import {
  caretIsAtSectionEnd,
  caretIsAtSectionStart, nextSectionExists,
  previousSectionExists, selectedSectionId,
  selectedSectionWords
} from './helpers/VuexGetters';
import store from '../store';

export default class ModificationService {
  keyPress: KeyPressService = null;
  selection: SelectService = null;
  sectionWords: WordInterface[] = null;
  modifiedSectionWords: WordInterface[] = null;
  moveCursor: number = null;

  keyPressed = (): string => { return this.keyPress.keyPressed; };
  dontMoveCursor: boolean = null;

  startWordId = (wordId?: number): number => { if (typeof wordId !== 'undefined') {this.selection.start.wordId = wordId;} else {return this.selection?.start?.wordId;} };
  startWord = (): WordInterface => { return !isNaN(this.startWordId()) ? this.sectionWords[this.startWordId()] : null; };
  startWordText = (wordContent?: string): string => { if (typeof wordContent !== 'undefined') {this.startWord().content = wordContent} else { return this.startWord()?.content; }};
  startSeparatorText = (separatorText?: string): string => { if (typeof separatorText !== 'undefined') {this.startWord().separator = separatorText === "" ? null : separatorText;} else {return this.startWord()?.separator || ''; }};
  startWordTextWithSeparator = (): string => { return this.startWordText() + this.startSeparatorText(); };
  startOffset = (offset?: number): number => { if (typeof offset !== 'undefined') {this.selection.start.offset = offset;} else {return this.selection.start?.offset;} };
  startSeparatorOffset = (): number => { return Math.max(this.startOffset() - this.startWordText().length, 0); };
  originalStartWordText: string = null;

  modifiedStartWord = () => {return this.modifiedSectionWords[this.startWordId()]};
  modifiedStartWordText = () => {return this.modifiedStartWord().content};
  modifiedStartWordSeparator = () => {return this.modifiedStartWord().separator || ''};
  modifiedStartWordTextWithSeparator = (): string => { return this.modifiedStartWordText() + this.modifiedStartWordSeparator(); };

  endWordId = (wordId?: number): number => { if (typeof wordId !== 'undefined') {this.selection.end.wordId = wordId;} else { return this.selection.end.wordId; } };
  endWord = ():WordInterface => { return this.sectionWords[this.endWordId()]; };
  endWordText = (wordContent?: string): string => { if (typeof wordContent !== 'undefined') { this.endWord().content = wordContent; } else { return this.endWord().content; }};
  endSeparatorText = (separatorText?: string): string => { if (typeof separatorText !== 'undefined') {this.endWord().separator = separatorText === "" ? null : separatorText;} else {return this.endWord().separator || ''; }};
  endWordTextWithSeparator = (): string => { return this.endWordText() + this.endSeparatorText(); };
  endOffset = (offset?: number): number => { if (typeof offset !== 'undefined') {this.selection.end.offset = offset;} else {return this.selection.end.offset;} };
  endSeparatorOffset = (): number => { return Math.max(this.endOffset() - this.endWordText().length, 0); };

  SEPARATOR_BECOMES_CONTENT = true;

  supportedFunctionalKeys = ["Delete", "Backspace", "Enter"];

  /**
   * @param {KeyPressService} keyPress
   * @param {SelectService} selection
   * @param {WordInterface[]} sectionWords
   */
  constructor(keyPress: KeyPressService, selection: SelectService) {
    const sectionWords = selectedSectionWords();
    if (!keyPress || !selection || !sectionWords) {
      throw new Error('initial data missing in ModificationService');
    }
    this.keyPress = keyPress;
    this.selection = selection;
    this.sectionWords = sectionWords;

    this.originalStartWordText = this.startWordText();

    this.dontMoveCursor = this.keyPressed() === "Delete" || this.keyPressed() === "Backspace" && this.selection.range ? true : false;
  };

  separatorDeleteRequest = () => {
    return !this.selection.range
      &&
      (
        (this.startOffset() >= this.startWordText().length && this.startOffset() < this.startWordTextWithSeparator().length && this.startSeparatorText() !== '' && this.keyPressed() === 'Delete')
        ||
        (this.startOffset() > this.startWordText().length && this.startSeparatorText() !== '' && this.keyPressed() === 'Backspace')
      );
  };

  separatorInsertRequest = () => {
    const separatorKeys = [',', '.', '!', '?', ':', ';', '-', ' '];

    return separatorKeys.indexOf(this.keyPressed()) !== -1
      && this.startOffset() >= this.startWordText().length
      && !(this.startOffset() === this.startWordText().length && this.keyPressed() === ' ')
      && !this.selection.range;
  };

  contentChangeRequest = () => {
    return this.keyPressed().length === 1 || this.supportedFunctionalKeys.indexOf(this.keyPressed()) !== -1;
  };

  /*
   * jeigu yra pažymėta skyrybos ženklo dalis, tuomet ji tampa contento dalim
   */
  separatorJoinsContent = () => {
    const startSeparatorOffset = this.startSeparatorOffset();
    this.startWordText(this.startWordText() + this.startSeparatorText().slice(0, startSeparatorOffset));
    this.startSeparatorText(this.startSeparatorText().slice(startSeparatorOffset, this.startSeparatorText().length));

    const endSeparatorOffset = this.endSeparatorOffset();
    this.endWordText(this.endWordText() + this.endSeparatorText().slice(0, endSeparatorOffset));
    this.endSeparatorText(this.endSeparatorText().slice(endSeparatorOffset, this.endSeparatorText().length));
  };

  /**
   * gaunami modifikuoti žodžiai po vartotojo veiksmo
   */
  getModifiedSectionWords = (): WordInterface[] => {
    if (this.modifiedSectionWords) {
      return this.modifiedSectionWords;
    }

    //jeigu paspaustas separatoriaus simbolis ir esam žodžio gale, pakeitimai visai kiti
    if (this.separatorDeleteRequest() || this.separatorInsertRequest()) {
      this.makeSeparatorChanges()
    } else if (this.keyPressed() === "Enter") {
      store.dispatch('splitSection');
      this.sectionWords = selectedSectionWords();
      !this.modifiedSectionWords && this.cloneSectionWords();
      this.selection.moveCursorToStartOfNextSection();
      this.moveCursor = 0;
    } else if (this.keyPressed() === "Backspace" && caretIsAtSectionStart() && previousSectionExists()) {
      this.selection.moveCursorToEndOfPreviousSection();
      const previousSectionId = selectedSectionId() - 1;
      const previousSectionWords = selectedSectionWords(previousSectionId);
      const activeSectionWords = selectedSectionWords();
      store.dispatch('mergeWithPreviousSection');
      this.sectionWords = previousSectionWords.concat(activeSectionWords);
      !this.modifiedSectionWords && this.cloneSectionWords();
      this.moveCursor = 0;
    } else if (this.keyPressed() === "Delete" && caretIsAtSectionEnd() && nextSectionExists()) {
      store.dispatch('mergeWithNextSection');
      this.sectionWords = selectedSectionWords();
      !this.modifiedSectionWords && this.cloneSectionWords();
      this.moveCursor = 0;
    } else if (this.contentChangeRequest()) {
      this.separatorJoinsContent();
      this.keyPressed().length > 1 && this.reactOnAction(); //reaguojam į delete, backspace klavišus
      this.makeContentChanges();
    }
    if (this.moveCursor === null) {
      this.moveCursor = !this.dontMoveCursor ? this.modifiedStartWordTextWithSeparator().length - this.startWordTextWithSeparator().length : 0;
    }

    return this.modifiedSectionWords;
  };

  cloneSectionWords = () => {
    if (!this.modifiedSectionWords) {
      this.modifiedSectionWords = this.sectionWords.map(word => {return {...word}});
    }
  };

  /**
   * gaunamas kursoriaus poslinkis po žodžių modifikavimo
   */
  getCursorMove = (): number => {
    if (this.moveCursor === null) {
      this.getModifiedSectionWords();
    }
    return this.moveCursor || 0;
  };

  reactOnAction = () => {
    if (this.supportedFunctionalKeys.indexOf(this.keyPressed()) === -1) {
      return;
    }
    if (this.keyPressed() === "Delete" || (this.keyPressed() === "Backspace" && this.selection.range)) {
      this.correctEndWordPropsForDelete();
    } else if (this.keyPressed() === "Backspace") {
      this.correctStartWordPropsForBackspace();
    }
    this.moveCursor = 0;
  };

  /**
   * redaguojamas separatorius
   */
  makeSeparatorChanges = () => {
    if(!this.separatorInsertRequest() && !this.separatorDeleteRequest()) {
      return false;
    }
    !this.modifiedSectionWords && this.cloneSectionWords();
    const separatorOffset = this.startSeparatorOffset();
    if (this.separatorInsertRequest()) {
      const separator = this.startSeparatorText().slice(0, separatorOffset) + this.keyPressed() + this.startSeparatorText().slice(separatorOffset, this.startSeparatorText().length);
      this.modifiedSectionWords[this.startWordId()].separator = separator === "" ? null : separator;
    } else if (this.separatorDeleteRequest()) {
      if (this.keyPressed() === 'Delete') {
        const separator = this.startSeparatorText().slice(0, separatorOffset) + this.startSeparatorText().slice(separatorOffset + 1, this.startSeparatorText().length);
        this.modifiedSectionWords[this.startWordId()].separator = separator === "" ? null : separator;
      } else if (this.keyPressed() === 'Backspace') {
        const separator = this.startSeparatorText().slice(0, separatorOffset - 1) + this.startSeparatorText().slice(separatorOffset, this.startSeparatorText().length);
        this.modifiedSectionWords[this.startWordId()].separator = separator === "" ? null : separator;
      }
    }
  };

  /**
   * redaguojamas contentas
   */
  makeContentChanges = (insertContent?: string) => {
    insertContent = insertContent ? insertContent : this.keyPressed().length === 1 ? this.keyPressed() : '';

    !this.modifiedSectionWords && this.cloneSectionWords(); //jei reikia, sukuriam modifiedSectionWords

    const newContent = this.startWordText().slice(0, this.startOffset()) + insertContent +  this.endWordText().slice(this.endOffset(), this.endWordText().length);

    if (!this.startWord().modified) {
      const lowerMainWord = this.originalStartWordText.toLowerCase();
      if (lowerMainWord !== newContent) {
        this.modifiedSectionWords[this.startWordId()].modified = true;
      }
    }
    this.modifiedSectionWords[this.startWordId()].content = newContent;
    this.modifiedSectionWords[this.startWordId()].end = this.endWord().end;

    if (!newContent) {
      this.modifiedSectionWords[this.startWordId()].separator = null;
    }
    //iš modifiedSectionWords masyvo pašalinami ištrinti žodžiai
    this.modifiedSectionWords.splice(this.startWordId() + 1, this.endWordId() - this.startWordId());
    if (this.selection.range && this.moveCursor === null) {
      this.moveCursor = 1;
    }
  };

  //atliekamos reikalingos korekcijos teisingam Delete funkcijos suveikimui
  correctEndWordPropsForDelete = () => {
    if (!this.selection.range) { //žyma per vieną poziciją
      if (this.endWordTextWithSeparator().length === this.endOffset() && this.sectionWords.hasOwnProperty(this.endWordId() + 1)) {
        //kursorius žodžio gale ir tolesnis žodis egzistuoja
        this.endOffset(0);
        this.endWordId(this.endWordId() + 1);
        this.startSeparatorText(this.endSeparatorText());

        if (this.SEPARATOR_BECOMES_CONTENT) {
          this.startWordText(this.startWordTextWithSeparator());
        }

        this.startOffset(this.startWordText().length);

      } else if (this.endWordText().length !== this.endOffset()) { //kursorius nėra žodžio gale
        this.endOffset(this.endOffset() + 1);
      }
    }
  };

  //atiliekamos reikalingos korekcijos teisingam Backspace funkcijos suveikimui
  correctStartWordPropsForBackspace = () => {
    if (!this.selection.range) { //žyma per vieną poziciją
      if (this.startOffset() === 0 && this.sectionWords.hasOwnProperty(this.startWordId() - 1)) {
        //kursorius žodžio pradžioje ir ankstesnis žodis egzistuoja
        this.startWordId(this.startWordId() - 1);
        this.originalStartWordText = this.startWordText();
        this.startSeparatorText(this.endSeparatorText());

        if (this.SEPARATOR_BECOMES_CONTENT) {
          this.startWordText(this.startWordTextWithSeparator());
        }

        this.startOffset(this.startWordText().length);

      } else if (this.startOffset() !== 0) { //kursorius nėra žodžio pradžioje
        this.startOffset(this.startOffset() - 1);
      }
    }
  };
}