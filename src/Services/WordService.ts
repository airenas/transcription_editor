import {WordAttributesInterface, WordInterface} from '@/Interfaces/WordInterface';
import {replaceGuessedMarks, replaceSpaceSeparator, wordIsGuessed} from "@/Services/helpers/SpaceSeparator";

export default class WordService {

    SKIP_WORDS: string[] = ["<eps>"];

  /**
   * Žodžio eilutė išskaidoma atributais. Grąžinamos tik tinkamo formato eilutės
   * @param {string} line
   * @returns WordInterface
   */
  getAttrs(line: string): WordAttributesInterface {
    let wordArray = line.split(' ');
    if (wordArray.length < 4) {
      return null;
    }
    const content = replaceSpaceSeparator(wordArray[3]);

    const wordAttrs = {
      auto: parseInt(wordArray[0]) as 0 | 1,
      start: parseFloat(wordArray[1]),
      end: parseFloat(wordArray[2]),
      content: replaceGuessedMarks(content),
      separator: wordArray.hasOwnProperty(4) ? replaceSpaceSeparator(wordArray[4]) : null,
      modified: false,
      guessed: wordIsGuessed(content)
    }

    return wordAttrs;
  }

    /**
     * sugeneruojamas žodžio objektas
     * @param words
     */
    generateWordObject (words: WordAttributesInterface[]): WordInterface {
      if (!words) {
        return null;
      }
      let word: WordInterface = {
        start: null,
        end: null,
        content: null,
        separator: null,
        modified: false,
        guessed: false
      };
      for (const w of words) {
        word.content = w.auto ? w.content : word.content;
        word.start = !word.start || word.start > w.start ? w.start : word.start;
        word.end = !word.end || word.end < w.end ? w.end : word.end;
        word.separator = w.separator ? w.separator : word.separator;
        word.guessed = w.auto === 1 ? w.guessed : word.guessed;
      }

      if (!word.content || this.SKIP_WORDS.includes(word.content)) {
        return null;
      }

      return word;
    }
}