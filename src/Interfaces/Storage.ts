export interface StorageEntity {
  email: string,
  event: string,
  file_name: string,
  id: number,
  registered: string,
  status: string
}

export interface StorageTranscriptionEntity {
  id: number,
  status: string,
  registered: string,
  error: {
    code: number,
    debug_message: string
  },
  transcription : {
    text: string,
    lattice: string
  }
}

export interface StoragePhonogram {
  email: string,
  data: string,
  file_name: string,
  job_type: string,
  event: string,
  record_quality: string,
  number_of_speakers?: number
}