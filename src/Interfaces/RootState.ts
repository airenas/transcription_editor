import {DeletedSectionInterface, SectionInterface} from './SectionInterface';
import UndoService from "@/Services/UndoService";
import SelectService from "@/Services/SelectService";

export interface RootState {
    media: HTMLMediaElement,
    mediaObjectUrl: string,
    mediaIsPlaying: boolean
    currentTime: number,
    sections: SectionInterface[],
    currentPage: number,
    perPage: number,
    mediaLength: number,
    playingMediaStart: number,
    playingMediaEnd: number,
    needToSelectPlayingSegments: boolean,
    transcriptionLength: number,
    selectedWordIds: number[],
    selectedSectionId: number,
    fileName: string,
    undoQueue: UndoService[],
    redoQueue: UndoService[],
    caretPosition: SelectService,
    speakers: string[],
    email: string,
    loadedStorageEntryId: number,
    deletedSection: DeletedSectionInterface,
    insertedSectionId: number,
    modalVisible: boolean,
    extendPlayingRange: boolean,
    scrollIntoViewEnabled: boolean
}