import { WordInterface } from './WordInterface';

export interface SectionInterface {
    id: number;
    speaker: string;
    start: number;
    end: number;
    words: WordInterface[];
}

export interface DeletedSectionInterface {
    key: number;
    section: SectionInterface
}