
export interface WordAttributesInterface {
  auto: 0 | 1;
  start: number;
  end: number;
  content: string;
  separator: string;
  modified: boolean;
  guessed: boolean;
}

export interface WordInterface {
  start: number;
  end: number;
  content: string;
  separator: string;
  modified: boolean;
  guessed: boolean;
}

export interface InsertedWordInterface {
  id: number;
  content: string;
}

export interface DeletedWordInterface extends InsertedWordInterface{}