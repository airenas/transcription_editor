module.exports = {
    // options...
    runtimeCompiler: true,
    devServer: {
        // disableHostCheck: true
    },

    publicPath: './',

    // where to output built files
    outputDir: 'dist',

    transpileDependencies: [
        'vue-confirm-dialog',
        'vue-context'
    ],

    pluginOptions: {
      i18n: {
        locale: 'en',
        fallbackLocale: 'en',
        localeDir: 'locales',
        enableInSFC: false
      }
    }
}
